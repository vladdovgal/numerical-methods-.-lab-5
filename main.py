import numpy as np
import matplotlib.pyplot as plt


def coef(x, y):
    n = len(x)
    a = []
    for i in range(n):
        a.append(y[i])

    for j in range(1, n):

        for i in range(n - 1, j - 1, -1):
            a[i] = float(a[i] - a[i - 1]) / float(x[i] - x[i - j])
    return np.array(a)  # повертає масив коефіцієнтів


def Eval(a, x, r):
    #    a : масив коефіцієнтів
    #    r : точка інтерполяції
    n = len(a) - 1
    temp = a[n] + (r - x[n])
    for i in range(n - 1, -1, -1):
        temp = temp * (r - x[i]) + a[i]
    return temp  # повертає наближене (інтерпольоване значення)




x = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
yA = [0, 0.09983, 0.19866, 0.29552, 0.38941, 0.47942, 0.56464, 0.64421, 0.71735, 0.78332]
yB = [2, 1.95533, 1.82533, 1.62160, 1.36235, 1.07073, 0.77279, 0.49515, 0.26260, 0.09592]

rA = [0.052, 0.303, 0.891]
rB = [0.122, 0.554, 0.812]

coefsA = coef(x, yA)
coefsB = coef(x, yB)

valuesA = [Eval(coefsA, x, rA[0]), Eval(coefsA, x, rA[1]), Eval(coefsA, x, rA[2])]
valuesB = [Eval(coefsB, x, rB[0]), Eval(coefsB, x, rB[1]), Eval(coefsB, x, rB[2])]

print('Коефіцієнти інтерполяційного многочлена Ньютона для функції A: ', coefsA, '\n')
print('Коефіцієнти інтерполяційного многочлена Ньютона для функції B: ', coefsB, '\n')

for i in valuesA:
    print("Значення ф-ї \"A\" в точці x = ", rA[valuesA.index(i)], " : ", i)
print()
for i in valuesB:
    print("Значення ф-ї \"B\" в точці x = ", rB[valuesB.index(i)], " : ", i)

# побудова графіків
plt.plot(x, yA, 'ro', rA, valuesA, 'bs')
plt.axis([0, 1, 0, 1])
for i in range (len(rA)):
    plt.axvline(rA[i], 0, 1, linestyle='dotted')
plt.show()

plt.plot(x, yB, 'go', rB, valuesB, 'bs')
for i in range (len(rB)):
    plt.axvline(rB[i], 0, 1, linestyle='dotted')
plt.axis([0, 1, 0, 2])
plt.show()